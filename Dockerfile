from jeffdesouza/jenkins-build

COPY . /code
WORKDIR /code

CMD ["python", "manage.py", "runserver"]
