from rest_framework import serializers
from blogapp.models import Post, Comment

posts = Post.objects.all()
post_list = []

for post in posts:
    post_list.append((post, post.title,))


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('id', 'title', 'text')

    def to_representation(self, instance):
        data = super(PostSerializer, self).to_representation(instance)
        return data


class CommentSerializer(serializers.ModelSerializer):
    post = serializers.PrimaryKeyRelatedField(
        many=False, queryset=Post.objects.all())

    class Meta:
        model = Comment
        fields = ('id', 'email', 'subject', 'message', 'post')

    def to_representation(self, instance):
        comment = super(CommentSerializer, self).to_representation(instance)
        post = PostSerializer(instance.post).data
        comment['post'] = post
        return comment


class PostsSerializer(serializers.ModelSerializer):
    comments = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Comment.objects.all())

    class Meta:
        model = Post
        fields = ('id', 'title', 'text', 'comments')

    def to_representation(self, instance):
        post = super(PostsSerializer, self).to_representation(instance)
        comments = CommentSerializer(instance.comments, many=True).data
        for comment in comments:
            del comment['post']
        post['comments'] = comments
        return post
