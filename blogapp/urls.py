from django.conf.urls import url, include
from blogapp.views import PostViewSet, CommentViewSet

post_list = PostViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

post_detail = PostViewSet.as_view({
    'get': 'retrieve',
    'put': 'partial_update',
    'delete': 'destroy'
})

comment_list = CommentViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

comment_detail = CommentViewSet.as_view({
    'get': 'retrieve',
    'put': 'partial_update',
    'delete': 'destroy'
})

urlpatterns = [
    url(r'^posts/$', post_list, name='post-list'),
    url(r'posts/(?P<pk>[0-9]+)/$', post_detail, name='post-detail'),
    url(r'comments/$', comment_list, name='comment-list'),
    url(r'comments/(?P<pk>[0-9]+)/$', comment_detail, name='comment-detail')
]
