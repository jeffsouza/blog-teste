from django.db import models

# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=255, default='')
    text = models.TextField()

    def __str__(self):
        return self.title


class Comment(models.Model):
    email = models.CharField(max_length=100)
    subject = models.CharField(max_length=150)
    message = models.TextField(default='')
    post = models.ForeignKey(
        'post', related_name="comments", on_delete=models.CASCADE)

    def __str__(self):
        return self.subject
