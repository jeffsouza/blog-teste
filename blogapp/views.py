from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from blogapp.models import Post, Comment
from blogapp.serializers import PostsSerializer, CommentSerializer
from django_filters.rest_framework import DjangoFilterBackend

# Create your views here.


@api_view(['GET'])
def api_root(request, format=None):
    if request.method == 'GET':
        return Response({
            'posts': reverse('post-list', request=request, format=format),
            'comments': reverse('comment-list', request=request, format=format)
        })


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostsSerializer

    # def get(self, request, *args, **kwargs):
    #     return self.list(request, *args, **kwargs)

    # def post(self, request, *args, **kwargs):
    #     return self.create(request, *args, **kwargs)

    # def get_serializer_class(self):
    #     if self.action in ['list', 'retrieve']:
    #         return PostListSerializer
    #     elif self.action in ['create', 'partial_update']:
    #         return PostCreateSerializer


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('subject', 'email', 'post__title')

    # def create(self, request):
    #     print(request.data)
    #     serializer = self.serializer_class(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()

    # def get(self, request, *args, **kwargs):
    #     return self.list(request, *args, **kwargs)

    # def post(self, request, *args, **kwargs):
    #     return self.create(request, *args, **kwargs)


# class CommentDetailView(mixins.RetrieveModelMixin, generics.GenericAPIView):
#     queryset = Comment.objects.all()
#     serializer_class = CommentSerializer

#     def get(self, request, *args, **kwargs):
#         return self.retrieve(request, *args, **kwargs)
