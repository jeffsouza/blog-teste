from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework.test import APIClient

# Create your tests here.


class PostTests(APITestCase):
    def test_list_posts(self):
        client = APIClient()
        request = client.get('/posts/', format='json')
        self.assertEqual(request.status_code, 200)
